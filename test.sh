echo $CI_COMMIT_MESSAGE
echo $CI_COMMIT_TAG
echo $CI_COMMIT_REF_NAME

# OLD=2
# NEW=2.0.2
# MIN=2.0.2
# A=$(( $NEW-$MIN ))
# echo $A

# SVC=$(kubectl get virtualservice auth-services-routing -n prod)
# if [[ $SVC == '' ]]; then
# 	echo 'error'
# fi
# echo "
# apiVersion: networking.istio.io/v1alpha3
# kind: VirtualService
# metadata:
#   name: _APP_NAME_-routing
# spec:
#   hosts:
#   - _HOSTS_
#   gateways:
#   - my-ingress-gateway
#   http:
#   - match:
#     - uri:
#         prefix: /auth
#         #gunakan 2 uri jika 1 service lebih dari 1 module
#     - uri:
#         prefix: /profile
#     route:
#     - destination:
#         host: _APP_NAME_
#         subset: v1
#         " > routing.yaml


# echo "
# apiVersion: networking.istio.io/v1alpha3
# kind: DestinationRule
# metadata:
#   name: _APP_NAME_-destination
# spec:
#   host: _APP_NAME_
#   subsets:
#   - name: v1
#     labels:
#       version: v1
#   - name: v1-0-1
#     labels:
#       version: v1.0.1
#   - name: v1-0-2
#     labels:
#       version: v1.0.2
#   - name: v1-0-3
#     labels:
#       version: v1.0.3
#       " > destination.yaml